### What is this repository for? ###

* This repository is to install jenkins in server using docker compose.

### How do I get set up? ###

* Install Docker and docker compose using below links.
https://docs.docker.com/install/ --> docker
https://docs.docker.com/compose/install/ --> docker compose
* Create .env and modify the parameters as required.Sample data is in .env_sample
* Run the docker compose command to make the stack up
docker-compose up -d

### Who do I talk to? ###
* arjunm@goodbits.in
